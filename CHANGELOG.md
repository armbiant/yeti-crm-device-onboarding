
## 0.0.7 [06-20-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!9

---

## 0.0.6 [01-05-2022]

* 21.2 certification

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!8

---

## 0.0.5 [11-15-2021]

* Patch/dsup 1004

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!7

---

## 0.0.4 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!6

---

## 0.0.3 [06-11-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!5

---

## 0.0.2 [02-09-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!4

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---
