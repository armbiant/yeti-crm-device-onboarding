<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your artifact name -->
# IAG Device Onboarding

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview
This artifact will allow users to automate the IAG device onboarding process. Device backup will also run via internal database.
<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->

<table><tr><td>
  <img src="./images/workflow.png" alt="workflow" width="800px">
</td></tr></table>

<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 5 minutes

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`
* Itential Automation Gateway

## Requirements

This artifact requires the following:

<!-- Unordered list highlighting the requirements of the artifact -->
<!-- EXAMPLE -->
* Network device to onboard into IAG (tested with IOS).
<!-- * Ansible or NSO (with F5 NED) * -->

## Features

The main benefits and features of the artifact are outlined below.
* Allows device backup (using IAP database) as part of the onboarding process.
* Allows removing the device automatically from IAG.
* Error handling to cover cases of name-conflict, connection timeout, etc.
<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## Future Enhancements
* The scope of future enhancements to this artifact includes the ability to test and support different device types.
<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->

## How to Install

To install the artifact:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section. 
* The artifact can be installed from within App-Admin_Essential. Simply search for the name of your desired artifact and click the install button (as shown below).

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>

<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

To run the artifact:

* Navigate to the appropriate Automation Catalog entry and run manually.
* Complete all form fields and then submit.

<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->
